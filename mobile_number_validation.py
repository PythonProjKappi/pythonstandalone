import re 
  
def isValid(s): 
    
    #Pattern = re.compile("(0/91)?[7-9][0-9]{9}") 
    p = re.compile("[9][0-9]{9}") 
    return p.match(s)
  

s = input()
if (isValid(s)):  
    print ("Valid Number")      
else : 
    print ("Invalid Number") 
